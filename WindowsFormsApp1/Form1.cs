﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ClosedXML;
using System.Collections;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
            //MessageBox.Show(openFileDialog1.FileName);
            if(openFileDialog1.FileName == null || openFileDialog1.FileName.Length < 1)
            {
                MessageBox.Show("未选择文件");
                return;
            }

            textBoxFile.Text = openFileDialog1.SafeFileName;

            button2.Enabled = true;

            filepath = openFileDialog1.FileName;


        }

        string filepath;
        private void button2_Click(object sender, EventArgs e)
        {
            var wb = new ClosedXML.Excel.XLWorkbook(filepath);
            var ws = wb.Worksheet("新的工作表_1");


            var col = 7;
            var insCol = 27;
            //var hashtable = new Hashtable();
            var keyArrayList = new ArrayList();
            var startRow = 2;

            for (int i = startRow; /*i <= endRow*/; i++)
            {
                var value = ws.Cell(i, col).GetString();
                if (String.IsNullOrEmpty(ws.Cell(i, 1).GetString()))
                {
                    MessageBox.Show("结束 endline("+i+")");
                    break;
                } 
                if (String.IsNullOrEmpty(value))
                {
                    continue;
                }

                string key;
                var valueArray = new ArrayList();

                var allArray = value.Split(new char[3] { '【', '】', '：' });

                var allArr = filterEmptyItem(allArray);

             
                if (allArr.Count % 2 != 0)
                {
                    MessageBox.Show("第" + i + "行出错，请手动处理");
                    continue;
                }

                for (int line = 0; line < allArr.Count; line += 2)
                {
                    var mapKey = (string)allArr[line];
                    var instaCol = insCol;
                    if (!keyArrayList.Contains(mapKey))
                    {
                        keyArrayList.Add(mapKey);
                        ws.Cell(startRow - 1, keyArrayList.IndexOf(mapKey) + insCol).Value = mapKey;
                    }
                    instaCol = keyArrayList.IndexOf(mapKey);
                    ws.Cell(i, instaCol+insCol).Value = allArr[line+1];
                }
            }
            wb.Save();

        }

        public static ArrayList filterEmptyItem(string[] arr)
        {
            var array = new ArrayList();

            foreach (string item in arr)
            {
                if (!String.IsNullOrEmpty(item))
                {
                    array.Add(item);
                }
            }

            return array;
        }

        private void button3_Click(object sender, EventArgs e)
        {

            var wbName = "Sheet1";  // 总表  Sheet1
            var col_科目名称 = 7;
            var col_客商辅助核算 = 15;
            var col_部门档案 = 16;
            var col_投融资单位 = 17;
            var col_人员档案 = 18;
            var col_借方 = 9;
            var col_贷方 = 10;
            var row_start = 2;
            var test_col = 1;

            var row_save_start = 2;

            var col_科目编码_短_save = 1;
            var col_科目编码_save = 2;

            var col_科目名称_save = 3;
            var col_客商辅助核算_save = col_科目名称_save+1;
            var col_部门档案_save = col_科目名称_save + 2;
            var col_投融资单位_save = col_科目名称_save + 3;
            var col_人员档案_save = col_科目名称_save + 4;
            var col_借方_save = col_科目名称_save + 5;
            var col_贷方_save = col_科目名称_save + 6;


            List<SubjectsDetail> arrayList = new List<SubjectsDetail>();
            
            var wb = new ClosedXML.Excel.XLWorkbook(filepath);
            
            var ws = wb.Worksheet(wbName); 
            var wsList = wb.Worksheets;
            //wsList.Delete(wbName);
            foreach (var wstemp in wsList)
            {

                if(wstemp.Name == wbName)
                {
                    MessageBox.Show("忽略结果输出表。。。");
                    continue;
                }

                for (int i = row_start; /*i <= endRow*/; i++)
                {
                    if (String.IsNullOrEmpty(wstemp.Cell(i, test_col).GetString()))
                    {
                        MessageBox.Show("结束 endline(" + (i - row_start) + ")"+wstemp.Name);
                        break;
                    }
                    SubjectsDetail sub = new SubjectsDetail();
                    sub.col_科目名称 = wstemp.Cell(i, col_科目名称).GetString();
                    sub.col_客商辅助核算 = wstemp.Cell(i, col_客商辅助核算).GetString();
                    sub.col_部门档案 = wstemp.Cell(i, col_部门档案).GetString();
                    sub.col_投融资单位 = wstemp.Cell(i, col_投融资单位).GetString();
                    sub.col_人员档案 = wstemp.Cell(i, col_人员档案).GetString();
                    sub.col_借方 = wstemp.Cell(i, col_借方).GetString();
                    sub.col_贷方 = wstemp.Cell(i, col_贷方).GetString();

                    arrayList.Add(sub);
                }
             }
            MessageBox.Show("end 条目：" + arrayList.Count);
           
            Hashtable hashtable = new Hashtable();
            
            foreach (var item in arrayList)
            {
                if (!hashtable.ContainsKey(item.ALl_string))
                {
                    hashtable.Add(item.ALl_string, item);
                }
            }
            var array = new List<SubjectsDetail>();
            foreach (var item in hashtable.Values)
            {
                array.Add((SubjectsDetail)item);
            }

            array.Sort();
            MessageBox.Show("去重结果 : " + array.Count);
            for (int i = 0; i < array.Count; i++)
            {
                SubjectsDetail sub = array[i];
                ws.Cell(i+row_save_start,col_人员档案_save).Value = sub.col_人员档案;
                ws.Cell(i + row_save_start, col_客商辅助核算_save).Value = sub.col_客商辅助核算;
                ws.Cell(i + row_save_start, col_投融资单位_save).Value = sub.col_投融资单位;
                ws.Cell(i + row_save_start, col_科目名称_save).Value = sub.col_科目名称;
                ws.Cell(i + row_save_start, col_部门档案_save).Value = sub.col_部门档案;
                ws.Cell(i + row_save_start, col_科目编码_save).Value = sub.col_科目编码.ToString();
                ws.Cell(i + row_save_start, col_科目编码_短_save).Value = sub.col_科目编码_短;
                ws.Cell(i + row_save_start, col_贷方_save).Value = sub.col_贷方;
                ws.Cell(i + row_save_start, col_借方_save).Value = sub.col_借方;

            }

            wb.Save();
            MessageBox.Show("保存完毕~");
            Application.Exit();
        }

        private void button4_Click(object sender, EventArgs e)
        {

           // var needGetColList = new int[] {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16 };

            var needGetColList = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 11, 13, 14, 15, 16 };
            var needGetColNameList = new String[] { "月","日","凭证号","摘要", "科目编码", "科目名称","辅助项", "币种", "借方原币","贷方原币","科目自由项", "核销信息", "结算信息","内部交易信息" };

            var wbName = "";

            //var filepath = "C:\\Users\\pc\\Desktop\\014017111.xlsx";
            var wb = new ClosedXML.Excel.XLWorkbook(filepath);

            
            var wsList = wb.Worksheets;
            
            var inputF = new InputForm("请输入表名");
            var res = inputF.ShowDialog();
            if (res == DialogResult.Cancel)
            {
                return;
            }
            
            if (wbName.Length < 1)
            {
                wbName = wb.Worksheets.First().Name;
            }

            var ws = wb.Worksheet(wbName);

            string resSheetName = "result_"+DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss.fff");

            var resSheet = wb.AddWorksheet(resSheetName);
            

            var startRow = 1;

            resSheet.Cell(1, 1).Value = "年";

            for (int i = 0; i < needGetColNameList.Length; i++)
            {

                resSheet.Cell(1, i + 2).Value = needGetColNameList[i];
            }

            var emptyCount = 0;

            var year = "";

            var outStarRow = 2;

            for (int i = startRow; /*i <= endRow*/; i++)
            {
                var value = ws.Cell(i, 1).GetString();
                
                /// 
                if (String.IsNullOrEmpty(value))
                {
                    emptyCount++;
                    if (emptyCount  > 10)
                    {
                        MessageBox.Show("结束 endline(" +  i  + ")" + ws.Name);
                        break;
                    }
                    continue;
                }
                emptyCount = 0;

                if ("月".Equals(value))
                {
                    /// 往前倒两行 解析 年
                    /// 

                    var year_row = i - 2;
                    var year_v = ws.Cell(year_row, 1).GetString();

                    year = year_v.Substring(3, 5);

                    continue;
                }
                int m;
                try
                {
                    m = int.Parse(value);

                }
                catch (Exception)
                {
                    continue;
                }

                //                MessageBox.Show(m.ToString());
                resSheet.Cell(outStarRow, 1).Value = year;



                for (int i2 = 0; i2 < needGetColList.Length; i2++)
                {
                    var input_col = needGetColList[i2];
                    var input_value = ws.Cell(i, input_col);

                    resSheet.Cell(outStarRow, i2+2).Value = input_value;

                }
                outStarRow++;
            }

            wb.Save();
            MessageBox.Show("操作完毕~");
            Application.Exit();
        }
    }
}
