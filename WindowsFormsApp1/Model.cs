﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    class SubjectsDetail: IComparable
    {
        public string col_科目名称;
        public string col_客商辅助核算;
        public string col_部门档案;
        public string col_投融资单位;
        public string col_人员档案;
        public string col_科目编码;
        public string col_科目编码_短;
        public string col_借方;
        public string col_贷方;


        public string all_string;
        public UInt64 sort;



        public string ALl_string
        {
            get
            {
                if (all_string == null)
                {
                    all_string = col_科目名称 + col_客商辅助核算 +
                        col_部门档案 + col_投融资单位 + col_人员档案;
                }
                return all_string;
            }
        }

        public UInt64 Sort
        {
            get
            {
                if (sort == 0)
                {
                    var res = col_科目名称.Split('\\');
                    var resLast = Form1.filterEmptyItem(res);
                    var first = resLast[0];
                    col_科目编码 = (string)first;
                    col_科目编码_短 = ((string)first).Substring(0,4);
                    sort = UInt64.Parse((string)col_科目编码_短);
                }
                return sort;
            }
        }

        public int CompareTo(object obj)
        {
            return Sort.CompareTo(((SubjectsDetail)obj).Sort);
        }

        public override bool Equals(object obj)
        {
            if (base.Equals(obj))
            {
                return true;
            }
            if(obj is SubjectsDetail)
            {
                var subj = (SubjectsDetail)obj;
                if(subj.ALl_string == ALl_string)
                {
                    return true;
                }
            }
            return false;
        }

    }
}
